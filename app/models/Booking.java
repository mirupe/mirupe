package models;

import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Future;

import org.joda.time.DateTime;

import play.Logger;
import play.data.validation.Check;
import play.data.validation.CheckWith;
import play.data.validation.InFuture;
import play.data.validation.Required;
import play.db.jpa.Model;
import play.mvc.Before;


@Entity
public class Booking extends Model {
	
	public enum Status {
		Open,
		Confirmed
	}
	
	@ManyToOne
	@Required
	@CheckWith(NotOverlapping.class)
	public CSpot spot;
	
	@ManyToOne
	@Required
	public User booker;
	
	@InFuture
	@Required
	@CheckWith(DateCheck.class)
	public Date startDate;
	
	@InFuture
	@Required 
	@CheckWith(WithinCSpotDates.class)
	public Date endDate;
	
	@Required
	public Status status;
	
	public Booking(CSpot spot, User booker, Date startDate, Date endDate) {
		super();
		this.spot = spot;
		this.booker = booker;
		this.startDate = startDate;
		this.endDate = endDate;
		this.status = Status.Open;
	}
	
	/**
	 * @param u User
	 * @return all bookings that belong to a Cspot owned by the passed user
	 */
	public static JPAQuery ownedBy(User u) {
		return Booking.find("FROM Booking as B WHERE exists(from CSpot WHERE user = ? AND id = B.spot)", u);
	}
	
	public static List<Booking> findBookings(CSpot cspot) {
		return Booking.find("spot = ? AND startDate BETWEEN ? AND ?", cspot, cspot.startDate, cspot.endDate).fetch();
	}
	
	public static DateTime findFirstFreeDay(CSpot cspot) {
		List<Booking> bookings = Booking.find("spot = ? AND startDate BETWEEN ? AND ? order by endDate DESC", cspot, cspot.startDate, cspot.endDate).fetch(1);
		if(bookings.size() == 0)
			return null;
		
		Booking b = bookings.get(0);
		if(b != null)
			return new DateTime(b.endDate);
		else return null;
	}
	
	
	/**
	 * The start date is before the end date
	 */
	static class DateCheck extends Check {
        
        public boolean isSatisfied(Object booking, Object startDate) {
        	Booking b = (Booking) booking;
        	Date s = (Date) startDate;
        	return s.before(b.endDate) || s.equals(b.endDate);
        }
    }
    
	/**
	 * There is no overlapping existing booking for the time range provided
	 */
    static class NotOverlapping extends Check {

		@Override
		public boolean isSatisfied(Object b, Object cspot) {
			CSpot spot = (CSpot) cspot;
			Booking booking = (Booking) b;
			long count = Booking.count("spot = ? AND (startDate <= ? AND endDate >= ? OR startDate <= ? AND endDate >= ?)", 
					spot, booking.startDate, booking.startDate, booking.endDate, booking.endDate);
			
			setMessage("There is already a booking for this spot at the date");
			return count == 0;
			
		}
    }
    
    /**
     * Checks if the booking dates are within the cspot date range
     */
    static class WithinCSpotDates extends Check {

		@Override
		public boolean isSatisfied(Object booking, Object endDate) {
			Booking b = (Booking) booking;
			setMessage("The camping spot is not available on this date");
			return  b.startDate.after(b.spot.startDate) && b.startDate.before(b.spot.endDate) &&
					b.endDate.before(b.spot.endDate) && b.endDate.after(b.spot.startDate);
		}
    	
    }

	@Override
	public String toString() {
		return "Booking [spot=" + spot + ", booker=" + booker + ", startDate="
				+ startDate + ", endDate=" + endDate + "]";
	}

	
}
