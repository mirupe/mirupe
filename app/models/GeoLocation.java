package models;

/**
 * Pojo for storing a pair of latitude and longitude
 */
public class GeoLocation {
	public Float latitude;
	public Float longitude;
	public GeoLocation(Float la, Float lo) {
		latitude=la;
		longitude = lo;
	}
	@Override
	public String toString() {
		return "GeoLocation [latitude=" + latitude + ", longitude=" + longitude
				+ "]";
	}
	
	
}
