package models;

import javax.persistence.Lob;
import javax.validation.constraints.Min;

import play.data.validation.Max;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;

public class Review extends Model {
	@Required @Lob @MaxSize(500)
	public String text;
	@Required @MaxSize(75)
	public String subject;
	@Required @Max(5) @Min(1)
	public Integer rating;
	@Required
	public User user;
	@Required
	public CSpot cspot; 
}