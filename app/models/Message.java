package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class Message extends Model {
	
	@Required public String subject;
	@Lob
	@Required public String message;
	@Required public Date sentAt;
	@ManyToOne
	@Required public User sender;
	@ManyToOne
	@Required public User receiver;
}
