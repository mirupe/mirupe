package models;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import play.data.validation.Email;
import play.data.validation.Required;
import play.data.validation.Unique;
import play.db.jpa.Model;

@Entity
public class User extends Model {
	@Required public String name;
	public String firstName;
	public String lastName;
	public Date birthday;

	@Required public String password;
	@Required public String rePassword2;
	
	@Email
	@Required
	@Unique
	public String email;

	@OneToMany
	public List<CSpot> spots;
	
	//@OneToMany
	//@JoinTable( name="CSpot" )
	//public List<Booking> bookings;

	public User(String name, String firstName, String lastName, Date birthday,
			String email, String password, String rePassword2) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.email = email;
		this.password = password;
		this.rePassword2 = rePassword2;
	}
	
	public User(String name, String firstName, String lastName, Date birthday,
			String email, String password) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.email = email;
		this.password = password;
	}



	

	@Override
	public String toString() {
		return "User [name=" + name + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", birthday=" + birthday
				+ ", password=" + password + ", rePassword2=" + rePassword2
				+ ", email=" + email + "]";
	}






	public static User connect(String email, String password) {
	    return find("byEmailAndPassword", email, password).first();
	}
}
