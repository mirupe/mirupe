package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

import org.hibernate.annotations.CollectionOfElements;

import play.data.validation.Min;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Blob;
import play.db.jpa.Model;

@SuppressWarnings("deprecation")
@Entity 
public class CSpot extends Model {
	@ManyToOne
	@Required
	public User user;	
	@Required @Min(0) 
	public Float price;
	@Required 
	public Date startDate;
	@Required 
	public Date endDate;
	@Required @Min(0) 
	public Float longitude;
	@Required @Min(0) 
	public Float latitude;
	@Required @MaxSize(75) 
	public String header;
	@Required @MaxSize(2000) @Lob
	public String text;
//	@Required 
//	public Location location;
	@CollectionOfElements 
    public Set<Review> reviews;
	@Required @CollectionOfElements 
    public Set<Integer> utilities;
	@Required @CollectionOfElements
	public List<Blob> photos;
	
	@OneToMany
	public Set<Booking> bookings;
	
	public CSpot(Float price, Float longitude, Float latitude, String header, String text, User user, Date startDate, Date endDate) {
		this.price = price;
		this.longitude = longitude;
		this.latitude = latitude;
		this.header = header;
		this.text = text;
		this.photos = null;
		this.utilities = null;
		this.user = user;
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	/**
	 * Returns the CSpot's within the geo locations southWest and northEast
	 * @param southWest boundary south west
	 * @param northEast boundary north east
	 * @param header 
	 */
	public static List<CSpot> findGeoLocations(GeoLocation southWest, GeoLocation northEast, String header) {
		return find("lower(header) like ? AND latitude BETWEEN ? AND ? AND longitude BETWEEN ? AND ?", "%"+header.toLowerCase()+"%", southWest.latitude, northEast.latitude, southWest.longitude, northEast.longitude).fetch();
	}
	
	/**
	 * Returns the CSpot's of the provided user within the geo locations southWest and northEast
	 * @param southWest boundary south west
	 * @param northEast boundary north east
	 * @param user only return CSpots with this user
	 */
	public static List<CSpot> findGeoLocationsForUser(GeoLocation southWest, GeoLocation northEast, User user) {
		return find("user = ? AND latitude BETWEEN ? AND ? AND longitude BETWEEN ? AND ?", user, southWest.latitude, northEast.latitude, southWest.longitude, northEast.longitude).fetch();
	}
	
	/**
	 * Given a search string find the corresponding header from all camping spots
	 * @param search 
	 * @param page
	 * @param size
	 * @return camping spot
	 */
	public static List<CSpot> findByHeader(String search, Integer page, Integer size) {
		if(search.trim().length() == 0) {
            return CSpot.all().fetch(page, size);
        } else {
            return CSpot.find("lower(header) like ?1", "%"+search.toLowerCase()+"%").fetch(page, size);
        }
	}
	
	/**
	 * On delete the photo on the hard disk needs to be deleted
	 */
	@Override
	public void _delete() {
	   super._delete();
	   for(Blob photo: photos)
		  photo.getFile().delete();
	}

	@Override
	public String toString() {
		return "CSpot [user=" + user + ", price=" + price + ", startDate="
				+ startDate + ", endDate=" + endDate + ", longitude="
				+ longitude + ", latitude=" + latitude + ", header=" + header
				+ ", text=" + text + ", utilities=" + utilities + "]";
	}
	
}

