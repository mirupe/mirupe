package models;

import play.db.jpa.Model;
import play.data.validation.Min;
import play.data.validation.MaxSize;
import play.data.validation.Required;

public class Location extends Model {
	@Required 
	public String city;
	public String state;
	@Required
	public String country;
	@MaxSize(10)
	public String zip;
}
