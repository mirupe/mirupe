import play.*;
import play.jobs.*;
import play.test.*;
import java.util.*; 
import java.io.*;
import play.db.jpa.*;
import play.libs.*;

import play.templates.*; 
import play.vfs.*;
import models.*;
 
@OnApplicationStart
public class Bootstrap extends Job {
 
    public void doJob() {
       
    	// Check if the database is empty
        if(User.count() == 0) {
          Logger.info("Loading Initial Data.");
          Fixtures.loadModels("initial-data.yml");
          List<CSpot> CSpots = CSpot.findAll();
          for (CSpot cspot: CSpots) {
            Logger.info("Looking for files for spot: [" + cspot.header + "]");
            for (int i=0; true; i++) {
              VirtualFile vf = VirtualFile.fromRelativePath("/public/images/"
                  + JavaExtensions.camelCase(cspot.header) + "-" + i + ".jpg");
              File imageFile = vf.getRealFile();
              Logger.info(vf.relativePath());

              if (imageFile.exists()) {
                try {
                  Blob blobImage = new Blob();
                  blobImage.set(new FileInputStream(imageFile), MimeTypes.getContentType(imageFile.getName()));
                  cspot.photos.add(blobImage);
                  
                  Logger.info("File: [%s] Loaded", imageFile.getAbsolutePath());
                } catch (FileNotFoundException e) {
                  // this should never happen.
                }
              } else {
                Logger.info("Media Loaded for post [%s]: %d files.", cspot.header, i);
                break;
              }
              cspot.save();
            }
          }
        }
      }
    
 
}