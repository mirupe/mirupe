package controllers;

import models.*;
import play.Logger;
import play.db.jpa.Blob;
import play.mvc.Controller;
import play.mvc.With;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

public class Search extends BaseController  {
	
	/**
	 * Render page at start
	 */
	public static void index(boolean showMap) {
		render(showMap);
	}
	
	/**
	 * Given the search string search for the camping spot header containing the respective string
	 * @param search : string from search field
	 * @param size : chosen page size
	 * @param page : page number
	 */
    public static void list(String search, Integer size, Integer page) {
    	Logger.info("Search: %s Size %d Page %d", search, size, page);
        page = page != null ? page : 1;
        List<CSpot> cspots = CSpot.findByHeader(search, page, size);
        render(cspots, search, size, page);
    }
    
    /**
     * Render the camping spot that was chosen for the detailed view
     * @param id : camping spot identifier
     */
    public static void view(long id){
    	CSpot cspot = CSpot.findById(id);
    	DateTime start = Booking.findFirstFreeDay(cspot);
    	if(start == null)
    		start = DateTime.now();
    	renderArgs.put("startDate", start.plusDays(1).toString("yyyy-MM-dd"));
		renderArgs.put("endDate", start.plusDays(8).toString("yyyy-MM-dd"));
    	render(cspot);
    }
    
    /**
     * Render the small view for camping spots that is displayed within the map
     * @param id camping spot id
     */
    public static void small(long id){
    	CSpot cspot = CSpot.findById(id);
    	render(cspot);
    }
    
    /**
     * Returns a list of startDate, endDate couples that represent when a spot (id)
     * id occupied
     * @param id id of the camping spot
     */
    public static void bookedDates(long id) {
    	CSpot cspot = CSpot.findById(id);
    	List<Booking> bookings = Booking.findBookings(cspot);
    	LinkedList<BookingDate> dates = new LinkedList<BookingDate>();
    	for (int i = 0; i < bookings.size(); i++) {
    		dates.add(new BookingDate(bookings.get(i)));
		}
    	renderJSON(dates);
    }
    
    public static void renderImage(long id) {
    	renderImages(id, 0);
    }
    public static void renderImages(long id, int i) {
    	CSpot cspot = CSpot.findById(id);
    	Blob photo = cspot.photos.get(i);
    	Logger.info("id: %d, i %d Photo name: %s", id, i, photo.getFile().getName());
    	if (photo.get() != null) {
    		response.setContentTypeIfNotSet(photo.type());
    		renderBinary(photo.get());
    	} else {
    		renderBinary(new File("public/images/default_cspot.png"));
    	}
    }
    
    /**
	 * Returns the CSpot's within the geo locations southWest and northEast
	 * @param southWest
	 * @param northEast
	 */
	public static void geoLocations(GeoLocation southWest, GeoLocation northEast, String header) {
		
		List<CSpot> spots = CSpot.findGeoLocations(southWest, northEast, header);
		renderJSON(spots);
	}
	
	public static class BookingDate {
		public Date startDate;
		public Date endDate;
		public BookingDate(Booking b) {
			this.startDate = b.startDate;
			this.endDate = b.endDate;
		}
	}
}
