package controllers;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.joda.time.DateTime;

import models.Booking;
import models.CSpot;
import models.User;
import models.GeoLocation;
import play.Logger;
import play.Play;
import play.data.Upload;
import play.data.validation.Valid;
import play.db.jpa.Blob;
import play.db.jpa.GenericModel.JPAQuery;
import play.mvc.*;

@With(Secure.class)
public class Listings extends BaseController {


	/**
	 * Render page at start
	 */
	public static void index() {
		DateTime now = DateTime.now();
		
		JPAQuery q = Booking.ownedBy(getCurrentUser());
		q.sq += " AND startDate BETWEEN ? AND ? ORDER BY spot, startDate DESC";
		Logger.info(q.sq);
		List<Booking> upcomingGuests = Booking.find(q.sq, getCurrentUser(), now.toDate(), now.plusWeeks(2).toDate()).fetch();
		Logger.info("Bookings count: %d", upcomingGuests.size());
		List<Booking>  upcomingVisits = Booking.find("booker = ? AND startDate BETWEEN ? AND ? ORDER BY spot, startDate DESC", getCurrentUser(), now.toDate(), now.plusWeeks(2).toDate()).fetch();
		render(upcomingGuests, upcomingVisits);
	}
	
	/**
	 * Render page at spot creation
	 */
	public static void newCSpot() {

		DateTime start = DateTime.now();
    	renderArgs.put("startDate", start.plusDays(0).toString("yyyy-MM-dd"));
		renderArgs.put("endDate", start.plusMonths(3).toString("yyyy-MM-dd"));

		render();
	}
	
	/**
	 * Create camping spot listing
	 * @param spot : camping spot
	 */
	public static void create(CSpot spot) {
		
		// The play framework does not handle the images correctly, since we have no
		// chance of naming the input field with multiple file select the way play
		// wants it to be (spot.photos[0], spot.photos[1], ...)
		// It just overwrites the first photo creating a duplicate of it. So we remove 
		// all photos and store them properly afterwards
		for(Object b : spot.photos) {
			// if no photo is selected, weirdly there is one Object in photos instead of a Blob
			if(b instanceof Blob) 
				((Blob) b).getFile().delete();
		}
		spot.photos = new ArrayList<Blob>();
		
	    List<Upload> files = (List<Upload>) request.args.get("__UPLOADS");
	    for(Upload file: files) {
	    	Blob image = new Blob();
	    	image.set(file.asStream(), file.getContentType());
	    	Logger.info("Created data/%s", image.getFile().getName());
	    	spot.photos.add(image);
	    }
	    
		spot.user = getCurrentUser();
		validation.valid(spot);
		if (validation.hasErrors()) {
			params.flash();
			validation.keep();
			flash.error("Invalid data. Please check the form.");
			newCSpot();
		}
		spot.save();
		flash.success("Successfully created a camping spot listing.");
		index();
	}

	/**
	 * Returns the CSpots of the current user within the geo locations southWest and northEast
	 * @param southWest
	 * @param northEast
	 */
	public static void userGeoLocations(GeoLocation southWest, GeoLocation northEast) {
		
		List<CSpot> spots = CSpot.findGeoLocationsForUser(southWest, northEast, getCurrentUser());
		renderJSON(spots);
	}
	
	
	public static String randColor() {
		Random random = new Random();
		final float saturation = 0.9f;//1.0 for brilliant, 0.0 for dull
		final float luminance = 1.0f; //1.0 for brighter, 0.0 for black
		Color color = Color.getHSBColor(random.nextFloat(), saturation, luminance);
		
		return Integer.toHexString(color.getRGB());	
	}
}
