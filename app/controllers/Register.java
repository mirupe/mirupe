package controllers;

import java.util.Date;

import models.User;
import play.Logger;
import play.mvc.*;
import play.libs.*;
import play.cache.*;
import play.data.validation.Required;

public class Register extends BaseController {

	/**
	 * Render page at start
	 */
	public static void index() {
		String randomID = Codec.UUID();
		render(randomID);
	}

	/**
	 * Create captcha image for user registration
	 * 
	 * @param id
	 */
	public static void captcha(String id) {
		Images.Captcha captcha = Images.captcha();
		String code = captcha.getText("#32cd32");
		Cache.set(id, code, "10mn");
		renderBinary(captcha);
	}

	/**
	 * Validate data to register user
	 * 
	 * @param code
	 * @param randomID
	 * @param name
	 * @param firstName
	 * @param lastName
	 * @param birthday
	 * @param password
	 * @param rePassword2
	 * @param email
	 */
	public static void create(@Required String code, String randomID,
			String name, String firstName, String lastName, Date birthday,
			String password, String rePassword2, String email) {
		User user = new User(name, firstName, lastName, birthday, email,
				password, rePassword2);
		validation.valid(user);
		validation.required(name);
		validation.required(email);
		validation.required(password);
		validation.required(rePassword2);
		validation.required(code);
		validation.email(email, email).message("Email not valid");
		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request
			render("Register/index.html", email);
		}
		validation.equals(rePassword2, password).message(
				"Passwords don't match");
		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request
			render("Register/index.html", rePassword2);
		}
		
		validation.equals(code, Cache.get(randomID)).message(
				"Invalid code. Please type it again");
		if (validation.hasErrors()) {
			Logger.info("capatcha falsch");
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request
			render("Register/index.html", randomID);
		}
		Cache.delete(randomID);

		if (validation.hasErrors()) {
			params.flash(); // add http parameters to the flash scope
			validation.keep(); // keep the errors for the next request
			index();
		} else
			user.save();
		// render(name, email, password, rePassword2);
		flash.success("Thank you for your registration. Please log-in with your credentials.");
		Application.index();
	}

}
