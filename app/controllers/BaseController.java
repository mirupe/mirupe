package controllers;

import models.User;
import play.mvc.Before;
import play.mvc.Controller;
import play.mvc.With;

/**
 * All our controllers should extend this class. It provides methods that are 
 * needed through the whole application. 
 */
public class BaseController extends Controller {
    
	/**
	 * If user is connected, set appropriate field
	 */
    @Before()
    static void setConnectedUser()
    {
    	renderArgs.put("isConnected", Security.isConnected());
    }
    
    /**
     * Finds the current user
     * @return current user
     */
    static User getCurrentUser() {
    	return User.find("byEmail", Security.connected()).first();
    }
}