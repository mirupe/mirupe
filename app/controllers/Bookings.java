package controllers;

import models.Booking;
import models.CSpot;
import play.Logger;
import play.data.validation.Error;
import play.mvc.With;

@With(Secure.class)
public class Bookings extends BaseController {

	/**
	 * Book a camping spot as the current user. Relies on validation of
	 * Booking that checks if the spot is occupied or free.
	 * 
	 * @param cspotid camping spot id
	 * @param booking new booking
	 */
	public static void create(long cspotid, Booking booking) {
		booking.spot = (CSpot) CSpot.findById(cspotid);
		booking.booker = getCurrentUser();
		booking.status = Booking.Status.Open;
		
		validation.valid(booking);
		if (validation.hasErrors()) {
			params.flash();
			validation.keep();
			for(play.data.validation.Error e : validation.errors()) 
				Logger.info("Error: %s : %s", e.getKey(), e.toString());
			Logger.info(validation.errors().toString());
			flash.error("Could not execute your booking!");
		} else {
			booking.save();
			flash.success("Successfully requested the camping spot!");
		}
		Search.view(booking.spot.id);
	}

	public static void delete(long bookingid) {
		Booking booking = Booking.findById(bookingid);
		if(booking != null && booking.spot.user.id == getCurrentUser().id) {
			booking.delete();
			flash.success("Successfully canceled booking!");
		}
		
		Listings.index();
	}
	
	public static void confirm(long bookingid) {
		Booking booking = Booking.findById(bookingid);
		if(booking != null && booking.spot.user.id == getCurrentUser().id) {
			booking.status = Booking.Status.Confirmed;
			booking.save();
			flash.success("Successfully canceled booking!");
		}
		
		Listings.index();
	}
}
