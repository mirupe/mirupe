$( function() {
	Mirupe.runIfOnPage('Listings.newCSpot', function() {
		
		var nowTemp = new Date();
		var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
		
		var checkin = $('#spot_startDate').datepicker({
			format: 'yyyy-mm-dd',
			onRender: function(date) {
				return date.valueOf() < now.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(ev) {
			if (ev.date.valueOf() > checkout.date.valueOf()) {
				var newDate = new Date(ev.date)
				newDate.setDate(newDate.getDate() + 1);
				checkout.setValue(newDate);
			}
			checkin.hide();
			$('#spot_endDate')[0].focus();
		}).data('datepicker');
		
		var checkout = $('#spot_endDate').datepicker({
			format: 'yyyy-mm-dd',
			onRender: function(date) {
				return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(ev) {
			checkout.hide();
		}).data('datepicker');
	});
});

$( function() {
	Mirupe.runIfOnPage(['Listings.index', 'Search.index'], function() {
		
		// load data from Listings.geoLocation controller / action
		// and display it
		google.maps.event.addListener(Mirupe.Map.googleMap, 'bounds_changed', Mirupe.Map.updateData );
	});
});

$( function() {
	Mirupe.runIfOnPage('Listings.newCSpot', function() {
		// How to get the location and display it:
	    var locationMarker = new google.maps.Marker({map: Mirupe.Map.googleMap});
	    locationMarker.setPosition(new google.maps.LatLng($("#latitude").attr('value'),$("#longitude").attr('value')));
	    Mirupe.Map.getLocation(function(location) {
	    	locationMarker.setPosition(location);
	    	$("#latitude").attr('value', location.lat());
	    	$("#longitude").attr('value', location.lng());
	    });
	});
});