if(!this.Mirupe) this.Mirupe = {};

// Run the callback only if body id equals page id
// page id may be an array of pageIds or a single string
Mirupe.runIfOnPage = function(pageId, callback) {
  var pages;
  if ($.isArray(pageId)) {
    pages = pageId;
  } else {
    pages = [pageId];
  }
  return $.each(pages, function(i, pageId) {
    if ($("body[id*='" + pageId + "']").length > 0) {
      return callback();
    }
  });
};

// init all date pickers
/*
$( function() {
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd'
	});
});
*/