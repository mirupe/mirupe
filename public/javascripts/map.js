if(!this.Mirupe) this.Mirupe = {};
Mirupe.Map = {};

$( function() {
	
	Mirupe.Map.googleMap = new google.maps.Map(document.getElementById("map_canvas"), {
      center: new google.maps.LatLng(48.14, 11.57),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    
	Mirupe.Map.getLocation = function(callback) {
		google.maps.event.addListener(Mirupe.Map.googleMap, 'click', function(event) {
			callback(event.latLng);
		});
	};
	
	Mirupe.Map.markers = [];
	Mirupe.Map.removeMarkers = function clearOverlays() {
		for (var i = 0; i < Mirupe.Map.markers.length; i++ ) {
			Mirupe.Map.markers[i].setMap(null);
		}
		Mirupe.Map.markers.length = 0;
	}
	
	Mirupe.Map.updateData = function() {
		// default endpoint is all CSpots of the current user
		var endpoint = 'listings/userGeoLocations';
		
		// change the endpoint if on page Search.index
		// to display CSpots of all users
		Mirupe.runIfOnPage('Search.index', function() { endpoint = "search/geoLocations"; });
		
    	var bounds = Mirupe.Map.googleMap.getBounds();
		var sw = bounds.getSouthWest();
		var ne = bounds.getNorthEast();
		// Request CSpots within boundaries
		$.getJSON( endpoint, {
			"southWest": {"latitude": sw.lat(), "longitude": sw.lng()}, 
			"northEast": {"latitude": ne.lat(), "longitude": ne.lng()},
			"header": $('#search').val()
		}, function( data ) {
			$.each(data, function(idx, cspot) {
				// add a marker to the map 
				var marker = new google.maps.Marker({
					map: Mirupe.Map.googleMap, 
					position: new google.maps.LatLng(cspot.latitude, cspot.longitude)
				});
				Mirupe.Map.markers.push(marker);
								
				// load the small view of the cspot and display it in an info window
				$.get( "/search/small/" + cspot.id, function( data ) {
											
					var infowindow = new google.maps.InfoWindow({
						content: data 					      
					});
					// on click of link detail in view small.html
					// change location to detail view
					$('#detail').on('click', function() {
						window.location = 'search/view/' + cspot.id;
					});
					
					// on click on markers go to the show page or info
					google.maps.event.addListener(marker, 'click', function() {
						infowindow.open(Mirupe.Map.googleMap, marker);
					});
				});
									
				
				google.maps.event.addListener(marker, 'dblclick', function() {
					window.location = 'search/view/' + cspot.id;
				});
			});
		});
    }
    
});