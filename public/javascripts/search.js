
$( function() {
	Mirupe.runIfOnPage('Search.index', function() {
		if($( "#searchForm" ).data("show-map")){
			$( "#searchForm" ).submit(function( event ) {
				event.preventDefault();
				if($('#search').val().length > 0) {
					Mirupe.Map.removeMarkers();
					Mirupe.Map.updateData();
				}
			});
		} else {
	 		$( "#searchForm" ).submit(function( event ) {
	 			event.preventDefault();
			 	  var $form = $( this ),
					search = $form.find( "input[name='search']" ).val(),
					size = $form.find( "select[name='size']" ).val(),
					page = $form.find( "input[name='page']" ).val(),
					url = $form.attr( "action" );
			 
				// Send the data using post
				var posting = $.post( url, { search: search, page: page, size: size } )
				.done(function( data ) {
					$( "#search_results" ).empty().append( data );
				});
			});
		}
	});
});	


//$( function() {
//	Mirupe.runIfOnPage('Search.index', function() {
//		$(document).ready(function() 
//			    { 
//			        $("#myTable").tablesorter({ 
//			            headers: { 
//			                // the first and third column (we start counting zero) do not sort
//			                0: { 
//			                    // disable it by setting the property sorter to false 
//			                    sorter: false 
//			                }, 
//			                2: { 
//			                    // disable it by setting the property sorter to false 
//			                    sorter: false 
//			                } 
//			            } 
//			        }); 
//			    });
//		});
//});	


$( function() {
	Mirupe.runIfOnPage('Search.view', function() {
 
	  $("#owl-demo").owlCarousel({
	    items : 2,
	    lazyLoad : true,
	    navigation : true
	  }); 
	});
});	

$( function() {
	Mirupe.runIfOnPage('Search.view', function() {
		
		$.getJSON("/search/booked_dates/" + $('#cspotid').val(), function(data) {
			
			var nowTemp = new Date();
			var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate() + 1, 0, 0, 0, 0);
			var spotStart = new Date($('#cspot').data('start_date'));
			var spotEnd = new Date($('#cspot').data('end_date'));
			
			
			var checkDate  = function(validDate, date) {

				// validate the current date in the picker by comparing it to the array
				// of booked dates
				var validate = function(selected) {
					var disabled = false;
					
					$.each(data, function(idx, item) {
						var start = new Date(item.startDate);
						var end = new Date(item.endDate);
						disabled = disabled || (selected.valueOf() <= end.valueOf() &&
							selected.valueOf() >= start.valueOf());
					});
					
					return disabled;
				};
				
				// date is before validDate
				disabled = date.valueOf() < validDate.valueOf();
				// spot is booked
				booked = validate(date);
				// spot is not active
				inactive = spotStart.valueOf() > date.valueOf() 
				 	|| spotEnd.valueOf() < date.valueOf();
				
				return (disabled || booked || inactive ? 'disabled ' : '') + ( booked ? ' booked' : '') + (inactive ? ' inactive' : '');
			}
			
			// disables startDate: 1. is the date in the past? 2. is the date booked?
			var checkin = $('#booking_startDate').datepicker({
				format: 'yyyy-mm-dd',
				onRender: function(date) {
					return checkDate(now, date);
				}
			}).on('changeDate', function(ev) {
				var newDate = new Date(ev.date)
				newDate.setDate(newDate.getDate());
				checkout.setValue(newDate);
				checkin.hide();
				$('#booking_endDate')[0].focus();
			}).data('datepicker');
			
			// disables endDate if it is before startDate
			var checkout = $('#booking_endDate').datepicker({
				format: 'yyyy-mm-dd',
				onRender: function(date) {
					return checkDate(checkin.date, date);
				}
			}).on('changeDate', function(ev) {
				checkout.hide();
			}).data('datepicker');
		});
	});
});
