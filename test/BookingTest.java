import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolationException;

import models.Booking;
import models.CSpot;
import models.User;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import play.Logger;
import play.data.validation.Validation;
import play.data.validation.Validation.ValidationResult;
import play.test.UnitTest;
import play.vfs.VirtualFile;


public class BookingTest extends UnitTest {
	
	@Before
	public void setUp() {
		Booking.deleteAll();
	}

	public void testDatesAreInFuture() {
		CSpot spot = (CSpot)CSpot.all().first();
		spot.startDate = DateTime.now().minusYears(10).toDate();
		spot.endDate = DateTime.now().plusYears(10).toDate();
		Booking b = new Booking(spot, (User)User.all().first(), DateTime.now().minusHours(1).toDate(), DateTime.now().minusMinutes(20).toDate());
		assertFalse(b.validateAndCreate());
	}
	
	@Test
	public void testEndDateBeforeStartDateIsInvalid() {
		Booking b = new Booking((CSpot)CSpot.all().first(), (User)User.all().first(), DateTime.now().plusDays(3).toDate(), DateTime.now().plusDays(2).toDate());
		assertFalse(b.validateAndCreate());
	}
	
	@Test
	public void testNoTwoBookingsInOneTimeRange() {
		
		CSpot spot = (CSpot)CSpot.all().first();
		spot.startDate = DateTime.now().toDate();
		spot.endDate = DateTime.now().plusYears(10).toDate();
		List<User> users = User.all().fetch(1);
		Booking first = new Booking(spot, users.get(0), DateTime.now().plusDays(2).toDate(), DateTime.now().plusDays(10).toDate());
		assertTrue(first.validateAndCreate());
		
		Booking overlapping  = new Booking(spot, users.get(0), DateTime.now().plusDays(3).toDate(), DateTime.now().plusDays(9).toDate());
		assertFalse(overlapping.validateAndCreate());
		
	}
	
	@Test 
	public void testStartEqualsEndDateIsValid() {
		CSpot spot = (CSpot)CSpot.all().first();
		spot.startDate = DateTime.now().minusYears(10).toDate();
		spot.endDate = DateTime.now().plusYears(10).toDate();
		Date same = DateTime.now().plusHours(1).toDate();
		Booking b = new Booking(spot, (User)User.all().first(), same, same);
		assertTrue(b.validateAndSave());
	}
	
	/**
	 * Book requests not in cspot start date - end date range will be rejected
	 */
	@Test
	public void testSpotRangeCheck() {
		CSpot spot = (CSpot)CSpot.all().first();
		spot.startDate = DateTime.now().plusDays(3).toDate();
		spot.save();
		List<User> users = User.all().fetch(1);
		
		// start date before cspot start date
		Booking wrongStartDate = new Booking(spot, users.get(0),  new DateTime(spot.startDate).minusHours(1).toDate(), DateTime.now().plusDays(10).toDate());
		assertFalse(wrongStartDate.validateAndCreate());
	}

}
