import org.joda.time.DateTime;
import org.junit.*;

import controllers.Search;

import java.util.*;
import play.test.*;
import models.*;

public class BasicTest extends UnitTest {

	/**
	 * Test if a new user can login
	 */
	@Test
	public void createAndRetrieveUser() {
	    new User("Max", null, null, null, "max@mustermann.de", "secret", "secret").save();
	    User bob = User.connect("max@mustermann.de", "secret");
	    
	    assertNotNull(bob);
	    assertEquals("Max", bob.name);
	}
	
	/**
	 * Test if new camping spots are saved
	 */
	@Test
	public void createCSpot() {
		long count = CSpot.count();
		new CSpot(1.0f, 48.16474f, 11.60486f, "abc", "some text", (User) User.all().first(), DateTime.now().toDate(), DateTime.now().toDate()).save();
		// there are 4 previous spots, now there should be 5
		assertEquals(count + 1, CSpot.count());
	}
	
	/**
	 * Test if the boundary search returns the correct amount of camping spots
	 */
	@Test
	public void findCSpotsWithinBoundaries() {
		// within this boundaries there are two camping spots
		GeoLocation southWest = new GeoLocation(48.10378885959707f, 11.485199279785093f);
		GeoLocation northEast = new GeoLocation(48.149621451781215f, 11.607078857421811f);
		
		List<CSpot> spots = CSpot.findGeoLocations(southWest, northEast);
		assertEquals(2, spots.size());
	}

	/**
	 * Test if the boundary search combinded with a user returns the correct spot
	 * owned by the user
	 */
	@Test
	public void findCSpotsForOneUserWithinBoundaries() {
		GeoLocation southWest = new GeoLocation(48.10378885959707f, 11.485199279785093f);
		GeoLocation northEast = new GeoLocation(48.149621451781215f, 11.607078857421811f);
		
		User user =  (User)User.all().fetch(1).get(0);
		List<CSpot> spots = CSpot.findGeoLocationsForUser(southWest, northEast, user);
		assertEquals(1, spots.size());
		assertEquals(user.id, spots.get(0).user.id);
	}
	
	/**
	 * The search for camping spot header should return headers like the passed header.
	 */
	@Test
	public void findCSpotsByHeader() {
		assertEquals(1, CSpot.findByHeader("abc", 1, 1000).size());
		assertEquals(13, CSpot.findByHeader("Place", 1, 1000).size());
		
	}
}
