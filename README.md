# Mirupe #

### Setup the project with eclipse:
Change direcotry to your eclipse workspace and clone the project. Then "eclipsify":
```
#!sh
cd workspace
git clone git@bitbucket.org:mirupe/mirupe.git
play eclipsify mirupe
```

Go to eclipse and select File -> Import... -> General -> Existing Projects into Workspace. Select your mirupe directory and you're done.